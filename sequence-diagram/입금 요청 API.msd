#//# --------------------------------------------------------------------------------------
#//# Created using Sequence Diagram for Mac
#//# https://www.macsequencediagram.com
#//# https://itunes.apple.com/gb/app/sequence-diagram/id1195426709?mt=12
#//# --------------------------------------------------------------------------------------
title "입금 요청 API"

participant "메시징 시스템" as msg
participant "카카오톡" as mobile
participant "뱅킹API" as us
participant "DBMS" as db
participant "코어뱅킹" as core
participant "금융결재원" as fin

msg-->mobile: 입금 요청 메시지 도착
note over mobile
"""
- 수취인의 당행고객 여부
- 수취인의 카카오 id
- 이체 id
- 메시지 내용
- 송금자가 작성한 수취인의 이름
- 입금 금액
"""
end note

### 입금 요청 시작


mobile->us: 입금 요청

note over us
"""
- 수취인의 당행고객 여부
- 수취인의 카카오 id
- 이체 id
- 송금자가 작성한 수취인의 이름
"""
end note

### 이체 상태 검증
us--> db: 이체 내역 조회
note over us
	"""
	- 이체 id
	- 이체 상태
	"""
end note

alt [이체 상태 검증: 취소 혹은 실패]
	us->mobile: 입금요청 결과반환: Fail (Cancel/Fail)
end


### 수취인이 당행 고객
alt [수취인이 당행 고객]
	activate db
	us-->db: 이체 내역 조회(lock)

	us-->db: 카카오 id 로 고객 찾기 요청
	db-->us: 고객 정보 반환
	
	note over us
		"""
		고객id
		고객 실명
		고객 대표 계좌
		"""
	end note
	
	### 실명 검증
	us-->us: 수취인 이름 검증
	alt [송금인이 작성한 수취인의 이름 != 고객 실명]
		us->db: 이체상태 "실패" 변경
		us->mobile: 당행입금 결과반환: FAIL
	end
	
	### 이체상태 임시 변경
	us->db: 이체상태 '처리중' 변경 후 lock 해제
	deactivate db
	
	us-->core: 고객 대표계좌로 입금 요청(대표계좌, 금액)
	core-->us: status code
	activate db
	us->db: 이체상태 '완료' 변경
	deactivate db
	us->mobile: 당행입금 결과반환: SUCCESS
deactivate db
end



alt [수취인이 타행 고객]
	mobile->us: 타행 종류 코드, 타행 계좌번호
	
	note over us
		"""
		- 타행 코드
		- 타행 계좌번호
		"""
	end note
	
	us-->core: 계좌 존재 조회
	core-->fin: 타행코드, 계좌번호로 계좌주 실명 조회 
	
	activate db
	us->db: 이체 내역 조회
	
	### 계좌 입력 횟수 검증
	alt [계좌 입력 오류 횟수 5회]
		us-->db: 이체 상태를 실패로 변경
		us->mobile: 계좌조회 결과반환: Fail
	end
	
	### 계좌가 존재하지 않음
	loop [계좌가 존재하지 않음]
		core-->us: Account Not Found
		us->db: 계좌입력횟수+1
		us-->mobile: 계좌재입력 요청
	end
	
	
	
	core-->us: 계좌 정보 반환 (계좌주이름)
	note over us
		"""
		- 계좌주 실명
		"""
	end note
	
	### 실명 검증
	us-->us: 수취인 실명 검증
	alt [수취인의 이름이 일치하지 않음]
		us->mobile: 당행입금 결과반환: Fail
	else

	us->db: 이체상태 '처리중' 변경 후 lock 해제
	deactivate db
	us-->core: 입금 요청(계좌, 금액)
	core-->fin: 타행 입금 요청
	fin-->core: status code
	core-->us: status code
	activate db
	us->db: 이체상태 '완료' 변경
	deactivate db
	us->mobile: 타행입금 결과반환: SUCCESS
	
	end
end alt









