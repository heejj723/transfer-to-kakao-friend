#//# --------------------------------------------------------------------------------------
#//# Created using Sequence Diagram for Mac
#//# https://www.macsequencediagram.com
#//# https://itunes.apple.com/gb/app/sequence-diagram/id1195426709?mt=12
#//# --------------------------------------------------------------------------------------
title "출금 요청 취소"

participant "모바일" as mobile
participant "뱅킹API" as us
participant "DBMS" as db
participant "수신" as core
participant "회계" as acc

mobile->us: 출금 취소 요청

note over us
	"""
	- 이체 id
	- 고객 id
	"""
end note


activate db
us-->db: 이체 가져오기 (with lock)

note over db
	"""
	상태 종류
	실패/취소/완료/대기중/독촉
	"""
end note


us-->us: 이체 상태 확인

alt [이체 상태가 '대기중'이 아님]
	us->mobile: 출금요청취소 결과반환: FAIL
end


us->db: 이체 상태 "처리중" 변경 후 lock 해제
deactivate db
us-->core: 이체 취소 요청
core-->acc: 돈통에서 송금자 계좌로 입금
core-->us: 결과 반환

activate db
us-->db: 이체 상태 '취소' 로 변경
deactivate db

us->mobile: 출금요청취소 결과반환: Success