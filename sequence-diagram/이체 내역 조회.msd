#//# --------------------------------------------------------------------------------------
#//# Created using Sequence Diagram for Mac
#//# https://www.macsequencediagram.com
#//# https://itunes.apple.com/gb/app/sequence-diagram/id1195426709?mt=12
#//# --------------------------------------------------------------------------------------
title "이체 내역 조회"

participant "모바일" as mobile
participant "뱅킹API" as us
participant "DB" as db

mobile-> us: 이체 내역 조회 요청

alt [조회 범위 == CUSTOM]

us->db: 조회범위, 이체 상태, 페이징 조건 쿼리

end

alt [조회 범위 != CUSTOM]

us->db: 조회범위(1달,3달) ~ 현재 까지의 이체 내역 조회
end

us->mobile: 쿼리 결과 반환