#//# --------------------------------------------------------------------------------------
#//# Created using Sequence Diagram for Mac
#//# https://www.macsequencediagram.com
#//# https://itunes.apple.com/gb/app/sequence-diagram/id1195426709?mt=12
#//# --------------------------------------------------------------------------------------
title "24시간 내 출금 취소"


participant "뱅킹API" as us
participant "데몬" as daemon
participant "DBMS" as db
participant "코어뱅킹" as core
participant "메시징" as msg

alt [여기까진 가정]
	us-->db: 이체 내역 저장
	note over db
		"""
		이체내역을 가지고 있음
		- 이체id
		- 송금자 고객id
		- 수취인 카톡id
		- 이체내역 생성 날짜
		- 이체 상태
		"""
	end note
end

alt [1. 23시간 경과이체 조회]
	activate db
	daemon-> db: 주기적으로 23시간이 지난 이체를 확인
	daemon-> db: 경고 플래그 = true
	deactivate db
	daemon-> msg: "이체를 받아주세요" 메시지 발송
end

alt [24시간 경과]
	activate db
	daemon->db: 24시간 지난 이체를 확인
	db->daemon: 24시간 지난 이체 리스트
	daemon->db: 리스트에 대해서 이체 상태를 "처리중" 으로 변경
	daemon->core: 송금자의 계좌로 입금 요청
	daemon->db: 리스트에 대해서 이체상태를 "취소" 로 변경
	deactivate db
end

