#//# --------------------------------------------------------------------------------------
#//# Created using Sequence Diagram for Mac
#//# https://www.macsequencediagram.com
#//# https://itunes.apple.com/gb/app/sequence-diagram/id1195426709?mt=12
#//# --------------------------------------------------------------------------------------
title "출금 요청 API"

participant "모바일" as mobile
participant "뱅킹API" as us
participant "DB" as db
participant "코어뱅킹" as core
participant "메시징" as msg

mobile--> us: 출금 요청

note over us
	"""
	- 고객id
	- 출금계좌번호
	- 수취인이름
	- 이체금액
	- 수취인카톡id
	- 수취인카톡uuid (친구 선택 시 얻어옴)
	"""
end note


us-->core: 대표계좌 출금 요청(대표계좌, 금액)
core-->us: 출금요청 결과반환

alt [출금요청-Success]
	us-->db: 간편 이체 내역 생성(이체상태: 입금대기중)
#	note over db
#	"""
#	이체 내역 생성
#	"""
#	end note
else
	us->mobile: 결과반환: FAIL
end

us--> db: 수취인 정보 조회
note over db
"""
카카오-고객 매칭 테이블을 확인하여
수취인이 당행 고객인지 확인한다.
"""
end note

### 수취인의 고객 여부에 따라
alt [수취인이 당행 고객 & BindActive]
	db-->us: 수취인 고객 id 반환
	us-->us: "카뱅으로받기" 메시지 생성
end

alt [수취인이 타행 고객]
	db-->us: X
	us-->us: "계좌를 입력하여 받기" 메시지 생성
end

note over us
"""
메시지 내용
(고객번호는 보내지 않음)
- 수취인의 카톡id
- 수취인 이름
"""
end note


us-->msg: 생성한 메시지를 수취인 카톡 uuid 와 함께 전달
note over msg
"""
- 수취인의 당행고객 여부
- 수취인의 카카오 id
- 생성 메시지
- 수취인의 카톡 uuid
- 이체 내역 id
"""
end note
msg-->us: 메시지 전송 결과

us->mobile: 결과 반환:SUCCESS
