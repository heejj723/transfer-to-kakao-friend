#//# --------------------------------------------------------------------------------------
#//# Created using Sequence Diagram for Mac
#//# https://www.macsequencediagram.com
#//# https://itunes.apple.com/gb/app/sequence-diagram/id1195426709?mt=12
#//# --------------------------------------------------------------------------------------
title "카카오톡 친구 이체내역 조회 API"

participant "모바일" as mobile
participant "뱅킹API" as us
participant "DBMS" as db

mobile-->us: 카톡 친구 이체내역 조회 요청
note over us
	"""
	- 당행고객 식별자
	"""
end note

us-->db: 

