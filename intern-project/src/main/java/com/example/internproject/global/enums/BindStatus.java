package com.example.internproject.global.enums;

public enum BindStatus implements CodeEnum{
    ACTIVE("ACTIVE"),
    IN_ACTIVE("IN_ACTIVE")
    ;

    private String bindStatus;
    BindStatus(String bindStatus) {
        this.bindStatus = bindStatus;
    }


    @Override
    public String getCode() {
        return this.bindStatus;
    }
}
