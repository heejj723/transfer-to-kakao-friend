package com.example.internproject.global.enums;

public enum ReceiverType implements CodeEnum{
    CLIENT("CLIENT"),
    NONE("NONE");

    private String receiverType;

    ReceiverType(String receiverType) {
        this.receiverType = receiverType;
    }

    @Override
    public String getCode() {
        return receiverType;
    }
}
