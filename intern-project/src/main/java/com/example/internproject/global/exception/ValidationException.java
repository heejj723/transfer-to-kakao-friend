package com.example.internproject.global.exception;

import lombok.Getter;

@Getter
public class ValidationException extends BusinessException{

    private final String code;
    private final String errorMsg;

    public ValidationException(ErrorCode errorCode) {
        this.code = errorCode.getCode();
        this.errorMsg = errorCode.getErrorMsg();
    }
}
