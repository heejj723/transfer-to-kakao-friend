package com.example.internproject.global.enums;

public enum KtTxStatus implements CodeEnum{
    COMPLETE("COMPLETE"),
    FAILURE("FAILURE"),
    CANCEL("CANCEL"),
    DEPOSIT_WAIT("DEPOSIT_WAIT"),
    IN_PROGRESS("IN_PROGRESS");

    private String ktTxStatus;

    KtTxStatus(String ktTxStatus) {
        this.ktTxStatus = ktTxStatus;
    }

    @Override
    public String getCode() {
        return ktTxStatus;
    }
}
