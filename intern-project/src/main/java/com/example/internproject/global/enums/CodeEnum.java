package com.example.internproject.global.enums;

public interface CodeEnum {
    String getCode();
}
