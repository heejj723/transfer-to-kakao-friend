package com.example.internproject.global.exception;

public enum ErrorCode {

    TRANSACTION_FAILURE("TX_FAIL", "Transaction failed"),
    INVALID_TX_STATUS("INVALID_TX_STATUS", "Transaction Status is not valid."),
    CLIENT_NOT_FOUND("CLIENT_NOT_FOUND", "Invalid user Id."),
    WRONG_RECEIVER_NAME("WRONG_RECEIVER_NAME", "Receiver name put by sender is invalid."),
    ACCOUNT_INPUT_TRY_LIMIT("ACCOUNT_INPUT_TRY_LIMIT", "Too many try to input deposit account."),
    INVALID_NAME_INPUT("INVALID_NAME_INPUT", "Receiver Name is Different"),
    TX_NOT_FOUND("TX_NOT_FOUND", "Transfer record not found"),
    ACCOUNT_NOT_FOUND("ACCOUNT_NOT_FOUND", "Input account is not found"),
    RECORD_LOCKED("RECORD_LOCKED", "Record Locked"),
    ACCOUNT_NOT_AVAILABLE("ACCOUNT_NOT_AVAILABLE", "Account is not available")
    ;

    private final String code;
    private final String errorMsg;

    ErrorCode(String code, String errorMsg) {
        this.code = code;
        this.errorMsg = errorMsg;
    }

    public String getCode() {
        return code;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
