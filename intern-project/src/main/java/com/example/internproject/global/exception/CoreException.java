package com.example.internproject.global.exception;


import lombok.Getter;

@Getter
public class CoreException extends BusinessException{
    private final String code;
    private final String errorMsg;

    public CoreException(ErrorCode errorCode) {
        this.code = errorCode.getCode();
        this.errorMsg = errorCode.getErrorMsg();
    }

}
