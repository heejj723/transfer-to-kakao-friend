package com.example.internproject.global.utils;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.sql.Time;
import java.sql.Timestamp;

//TODO: 테이블에 들어가기 직전에 Timestamp 로 자동으로 변환하게 해줘서 실제로 비즈니스 로직과 분리할 수 없을까?
public class EventConverter {

    @Component
    public static class TimestampConverter implements Converter<String, Timestamp>{
        @Override
        public Timestamp convert(String source) {
            return new Timestamp(Long.parseLong(source));
        }
    }

    @Component
    public static class TimeConverter implements Converter<String, Time>{
        @Override
        public Time convert(String source) {
            return new Time(Long.parseLong(source));
        }
    }
}

