package com.example.internproject.global.enums;

public enum TxType implements CodeEnum{
    GANPYUN("간편이체"),
    JUKSI("즉시이체")
    ;


    String txType;

    TxType(String txType) {
        this.txType = txType;
    }

    @Override
    public String getCode() {
        return txType;
    }
}
