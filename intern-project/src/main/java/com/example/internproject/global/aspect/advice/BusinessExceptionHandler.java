package com.example.internproject.global.aspect.advice;

import com.example.internproject.global.dto.CommonResponseDto;
import com.example.internproject.global.exception.BusinessException;
import com.example.internproject.global.exception.CoreException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class BusinessExceptionHandler {

    @ExceptionHandler({BusinessException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public CommonResponseDto<?> businessExceptionHandler(CoreException e) {
        log.error("Business Exception: [{}]-{}", e.getCode(), e.getErrorMsg());

        return CommonResponseDto.builder()
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .errorCode(e.getCode())
            .errorMessage(e.getMessage())
            .build();
    }
}
