package com.example.internproject.global.enums;

public enum DateRangeType implements CodeEnum{

    MONTH_1("1 MONTH"),
    MONTH_3("3 MONTH"),
    CUSTOM("CUSTOM")
    ;

    private String dateRangeType;

    DateRangeType(String dateRangeType) {
        this.dateRangeType = dateRangeType;
    }

    @Override
    public String getCode() {
        return dateRangeType;
    }
}
