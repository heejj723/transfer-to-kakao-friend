package com.example.internproject.global.enums;

public enum BankCode implements CodeEnum{

    KAKAO("카카오뱅크"),
    SHINHAN("신한"),
    KUKMIN("국민"),
    NONG_HYUP("농협"),
    HANA("하나");

    private String bankType;

    BankCode(String bankType) {
        this.bankType = bankType;
    }

    @Override
    public String getCode() {
        return bankType;
    }
}
