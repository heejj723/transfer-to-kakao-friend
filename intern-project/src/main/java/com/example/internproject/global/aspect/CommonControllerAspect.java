package com.example.internproject.global.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Aspect
public class CommonControllerAspect {


    @Around("execution(* com.example.internproject.domain.controller..*.*(..))")
    public Object commonControllerAspect(final ProceedingJoinPoint pjp) throws Throwable {
        String targetName = pjp.getTarget().getClass().toString();

        log.info( "{}: 시작", targetName);

        Object result = null;
        try {
            log.info( "{}: Try", targetName);

            result = pjp.proceed();

        } catch ( Exception e ) {
            log.info( "{}: Exception", targetName);
            throw e;

        } finally {
            log.info( "{}: Finally", targetName);
        }

        return result;
    }
}
