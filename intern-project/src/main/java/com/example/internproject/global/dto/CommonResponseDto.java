package com.example.internproject.global.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.Optional;

@Getter
@Builder
public class CommonResponseDto<T> {

    private final HttpStatus status;
    private final String errorCode;
    private final String errorMessage;
    private final T body;

    public static <T> CommonResponseDto<T> ok (T body) {
        return CommonResponseDto.<T> builder()
            .status(HttpStatus.OK)
            .errorCode("TBD")
            .errorMessage("TBD")
            .body(body)
            .build();
    }
}
