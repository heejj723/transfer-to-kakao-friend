package com.example.internproject.global.enums;

public enum MsgCardType implements CodeEnum{
    DEFAULT("DEFAULT"),
    BOB("BOB"),
    SUL("SUL"),
    YONG("YONG"),
    COFFEE("COFFEE"),
    TRANSPORT("TRANSPORT"),
    LOVE("LOVE"),
    THANK("THANK"),
    CELEBRATE("CELEBRATE"),
    WEDDING("WEDDING"),
    FUNERAL("FUNERAL");

    private String msgCardType;
    MsgCardType(String msgCardCode) {
        this.msgCardType = msgCardCode;
    }

    @Override
    public String getCode() {
        return msgCardType;
    }
}
