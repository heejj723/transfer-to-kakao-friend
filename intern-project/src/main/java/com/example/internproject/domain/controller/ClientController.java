package com.example.internproject.domain.controller;

import com.example.internproject.domain.dto.ClientReqDto;
import com.example.internproject.domain.model.Client;
import com.example.internproject.domain.service.ClientService;
import com.example.internproject.global.dto.CommonResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Pageable;
import java.util.List;

@RestController
@RequestMapping("/clients")
@RequiredArgsConstructor
public class ClientController {

    private final ClientService clientService;

    //todo: 리팩토링 필요 (모델 표현쪽에 노출)
    @GetMapping
    public CommonResponseDto<List<Client>> getAllClient() {
        return CommonResponseDto.ok(clientService.getAllClient());
    }

    @GetMapping("/{userId}")
    public CommonResponseDto<Client> getClient(@PathVariable Long userId) {
        return CommonResponseDto.ok(clientService.getClient(userId));
    }

    @PostMapping
    public CommonResponseDto<Long> create(@RequestBody ClientReqDto clientReqDto) {
        return CommonResponseDto.ok(clientService.create(clientReqDto));
    }
}
