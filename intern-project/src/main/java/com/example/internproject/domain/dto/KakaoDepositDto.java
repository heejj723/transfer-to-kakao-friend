package com.example.internproject.domain.dto;

import com.example.internproject.global.enums.BankCode;
import com.example.internproject.global.enums.KtTxStatus;
import com.example.internproject.global.enums.ReceiverType;
import lombok.*;
import lombok.experimental.SuperBuilder;


public class KakaoDepositDto {

    @Getter @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Req {

        private ReceiverType receiverType;
        private String kakaoUUID;
        private Long ktAccId;
        private Long txId;
        private BankCode bankCode;
        private String inAccount;
    }

    @Getter
    @Builder
    public static class Res {
        private Long txId;
        private KtTxStatus ktTxStatus;
        private Long amount;
    }

}
