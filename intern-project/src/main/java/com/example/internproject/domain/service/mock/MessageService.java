package com.example.internproject.domain.service.mock;

import com.example.internproject.global.enums.MsgCardType;
import com.example.internproject.global.enums.ReceiverType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MessageService {
    public void send(ReceiverType receiverType, Long txId, Long inKakaoId,
                     String uuid, String ktInMsg, MsgCardType msgCardType, Long amount) {

        log.info("메시지 보내기 실행, 성공");

    }
}
