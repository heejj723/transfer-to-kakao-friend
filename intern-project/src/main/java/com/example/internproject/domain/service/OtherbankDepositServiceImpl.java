package com.example.internproject.domain.service;

import com.example.internproject.domain.dao.KtTxRepository;
import com.example.internproject.domain.dto.KakaoDepositDto;
import com.example.internproject.domain.dto.dummy.AccountInfoDummyDto;
import com.example.internproject.domain.model.KtTx;
import com.example.internproject.domain.service.mock.MockService;
import com.example.internproject.global.enums.BankCode;
import com.example.internproject.global.enums.KtTxStatus;
import com.example.internproject.global.exception.CoreException;
import com.example.internproject.global.exception.ErrorCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class OtherbankDepositServiceImpl implements KakaoDepositService<KakaoDepositDto.Req, KakaoDepositDto.Res> {

    private final KtTxRepository ktTxRepository;
    private final Integer INPUT_TRY_COUNT_LIMIT = 4;
    private final MockService mockService;
    private final TransferValidator transferValidator;


    /** 타행 계좌로 입금
     * 0. tx_id 에 대해서 계좌 입력 횟수가 4회 이상인지 확인
     * 0-1. 4회 이상일 경우 TX_STATUS=FAIL, Exception
     * 1. 은행코드/계좌번호로 존재하는 계좌인지 확인.
     * 1-1. 존재하지 않는 계좌일 경우 계좌 재입력 요청
     * 1-2. 계좌 입력 시도 횟수 +1
     * 2. 계좌 정보가 반환 됨. (계좌주, 잔액, 입금 가능 계좌 여부)
     * 2-1. 입금 가능 계좌가 아닐 경우 exception
     * 2-2. 계좌주 실명 검증, 아닐 경우 FAIL, exception
     * 3. 코어에 타행 입금 요청
     * 4. 결과 반환
     * */

    @Transactional(noRollbackFor = RuntimeException.class)
    @Override
    public KakaoDepositDto.Res deposit(KakaoDepositDto.Req reqDto) {
        Long txId = reqDto.getTxId();

        /** 여기서부터 lock 걸려있음 */
        KtTx transfer = ktTxRepository.findByTxIdForOneDayForUpdateNowait(txId)
            .orElseThrow(()-> new CoreException(ErrorCode.TX_NOT_FOUND));

        // 입금 조건 만족 여부 확인
        checkDepositAvailable(transfer, reqDto);

        // 입금요청
        mockService.depositOk(reqDto.getBankCode().getCode(),
            reqDto.getInAccount(), transfer.getTxAmount());

        ktTxRepository.updateTxStatus(txId, KtTxStatus.COMPLETE.getCode());

        return KakaoDepositDto.Res.builder()
            .txId(txId)
            .ktTxStatus(KtTxStatus.COMPLETE)
            .amount(transfer.getTxAmount())
            .build();
    }

    private void checkDepositAvailable(KtTx transfer, KakaoDepositDto.Req reqDto) {
        // 이체 상태 체크
        checkValidTransfer(transfer);

        // 계좌 정보 받아오기
        AccountInfoDummyDto accountInfoDummyDto = getAccountInfo(reqDto.getTxId(),
            reqDto.getBankCode(), reqDto.getInAccount());

        // 사용가능 계좌인지 확인
        checkAccountAvailability(accountInfoDummyDto.getAvailable());

        // 이름 검증
        transferValidator.validateName(transfer.getInName(), accountInfoDummyDto.getName());
    }

    private void checkAccountAvailability(Boolean available) {
        if (!available) {
            throw new CoreException(ErrorCode.ACCOUNT_NOT_AVAILABLE);
        }
    }

    private KtTx checkValidTransfer(KtTx transfer) {
        if (transfer.getInAccInputTry() >= INPUT_TRY_COUNT_LIMIT) {
            log.info("5번의 입력 횟수 초과");
            ktTxRepository.updateTxStatusAndIncreaseTryCount(transfer.getTxId(), KtTxStatus.FAILURE.getCode());
            throw new CoreException(ErrorCode.ACCOUNT_INPUT_TRY_LIMIT);
        }
        return transfer;
    }

    private AccountInfoDummyDto getAccountInfo(Long txId, BankCode bankCode, String inAccount) {
        try {
            return mockService.getAccountInfoFail(bankCode.getCode(), inAccount);
        } catch (Exception e) {
            log.error("계좌 정보 요청 실패: 존재하지 않는 계좌");
            ktTxRepository.increaseTryCount(txId);
            throw new CoreException(ErrorCode.ACCOUNT_NOT_FOUND);
        }
    }




}
