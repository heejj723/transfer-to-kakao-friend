package com.example.internproject.domain.service;

import com.example.internproject.domain.dao.KtTxRepository;
import com.example.internproject.domain.dto.TransferHistoryDto;
import com.example.internproject.domain.model.KtTx;
import com.example.internproject.global.enums.DateRangeType;
import com.example.internproject.global.enums.KtTxStatus;
import com.example.internproject.global.enums.TxType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TransferHistoryService {

    private final KtTxRepository ktTxRepository;

    public List<TransferHistoryDto.Res> getTransferHistory(
        Long userId,
        String account,
        String startDt,
        String endDt,
        DateRangeType dateRangeType,
        KtTxStatus ktTxStatus,
        Integer pageNum,
        Integer pageSize) {

        List<KtTx> transferList;

        final long pageOffset = (pageNum.longValue()-1)*pageSize.longValue();

        // todo: 이부분 굳이 1달, 3달, 사용자 지정 이런식으로 나눠야하나? 그냥 입력값을 하나로만 받을 수는 없을까?
        if (!DateRangeType.CUSTOM.equals(dateRangeType)) {
            transferList = ktTxRepository.findAllConditionedSearchByDateType(
                userId,
                account,
                dateRangeType.getCode(),
                ktTxStatus.getCode(),
                pageOffset,
                pageSize
            );
        } else {

            transferList = ktTxRepository.findAllConditionedSearchByRange(
                userId,
                account,
                getDateFromString(startDt),
                getDateFromString(endDt),
                ktTxStatus.getCode(),
                pageOffset,
                pageSize
            );
        }

        return transferList.stream()
            .map(t ->
            new TransferHistoryDto.Res(t.getTxId(),
                t.getTxOutMsg(),
                KtTxStatus.valueOf(t.getKtTxStatus()),
                TxType.GANPYUN,
                t.getTxAmount(),
                getDateStringFromTs(t.getCreatedAt())))
            .collect(Collectors.toList());
    }

    //TODO: 테이블에 들어가기 직전에 Timestamp 로 자동으로 변환하게 해줘서 실제로 비즈니스 로직과 분리할 수 없을까?
    private String getDateStringFromTs(Timestamp ts) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        String result;
        try {
            result = sf.format(sf.parse(ts.toString()));
        } catch (ParseException e) {
            throw new RuntimeException();
        }
        return result;
    }

    private Date getDateFromString(String dateString) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        Date result;
        try {
            result = sf.parse(dateString);
        } catch (ParseException pe) {
            throw new RuntimeException();
        }
        return result;
    }
}
