package com.example.internproject.domain.controller;

import com.example.internproject.domain.dto.TransferHistoryDto;
import com.example.internproject.domain.service.TransferHistoryService;
import com.example.internproject.global.dto.CommonResponseDto;
import com.example.internproject.global.enums.DateRangeType;
import com.example.internproject.global.enums.KtTxStatus;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/transfer-history")
@ApiOperation("이체 내역 조회 API")
@RequiredArgsConstructor
public class TransferHistoryController {

    private final TransferHistoryService transferHistoryService;

    @ApiOperation("이체 내역 조회")
    @GetMapping
    public CommonResponseDto<List<TransferHistoryDto.Res>> getTransferHistory(
        @RequestParam(value = "userId") Long userId,
        @RequestParam(value = "account") String account,
        @RequestParam(value = "startDt", required = false) String startDt,
        @RequestParam(value = "endDt", required = false) String endDt,
        @RequestParam(value = "dateRangeType") DateRangeType dateRangeType,
        @RequestParam(value = "ktTxStatus") KtTxStatus ktTxStatus,
        @RequestParam(value = "pageNum") Integer pageNum,
        @RequestParam(value = "pageSize") Integer pageSize
        ) {
        return CommonResponseDto.ok(transferHistoryService.getTransferHistory(
            userId, account, startDt, endDt, dateRangeType, ktTxStatus, pageNum, pageSize));
    }
}
