package com.example.internproject.domain.service;

import com.example.internproject.domain.dao.KtTxRepository;
import com.example.internproject.domain.model.KtTx;
import com.example.internproject.global.enums.ReceiverType;
import com.example.internproject.global.exception.CoreException;
import com.example.internproject.global.exception.ErrorCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class KakaoDepositServiceFactory {

    private final KabangDepositServiceImpl kabangDepositServiceImpl;
    private final OtherbankDepositServiceImpl otherbankDepositServiceImpl;
    private final KtTxRepository ktTxRepository;
    private final TransferValidator transferValidator;

    public KakaoDepositService getService(Long txId, ReceiverType receiverType) {

        checkTxStatus(txId);
        if (ReceiverType.CLIENT.equals(receiverType)) {
            return kabangDepositServiceImpl;
        }
        return otherbankDepositServiceImpl;
    }

    /** 공통: 이체 상태 확인 및 Record Lock*/
    @Transactional
    void checkTxStatus(Long txId) {
        KtTx ktTx = ktTxRepository.findByTxIdForOneDayForUpdateNowait(txId)
            .orElseThrow(()-> new CoreException(ErrorCode.TX_NOT_FOUND));

        transferValidator.checkDepositWait(ktTx);
    }

}


