package com.example.internproject.domain.dao;

import com.example.internproject.domain.model.KtTx;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Mapper
public interface KtTxRepository {
    KtTx findByTxId(Long txId);
    Optional<KtTx> findByTxIdForOneDayForUpdateNowait(Long txId);
    Long create(KtTx ktTx);
    void updateTxStatusAndIncreaseTryCount(Long txId, String ktTxStatus);
    void increaseTryCount(Long txId);
    void updateTxStatus(Long txId, String ktTxStatus);

    List<KtTx> findAllConditionedSearchByRange(Long userId, String outAccount, Date startDate, Date endDate, String ktTxStatus,
                                               Long offset, Integer pageSize);
    List<KtTx> findAllConditionedSearchByDateType(Long userId, String outAccount, String dateRangeType, String ktTxStatus,
                                                  Long offset, Integer pageSize);
}
