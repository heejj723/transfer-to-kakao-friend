package com.example.internproject.domain.controller;

import com.example.internproject.domain.dto.KakaoAccountDto;
import com.example.internproject.domain.service.KtAccService;
import com.example.internproject.global.dto.CommonResponseDto;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/kt_acc")
@ApiOperation("카톡 계정 연동 API ")
@RequiredArgsConstructor
public class KtAccController {

    private final KtAccService ktAccService;


    @ApiOperation("카톡 계정과 고객 연동")
    @PostMapping("/connect")
    public CommonResponseDto<KakaoAccountDto.Res> connect(@RequestBody KakaoAccountDto.Req req) {
        KakaoAccountDto.Res res = ktAccService.connect(req);
        return CommonResponseDto.ok(res);
    }

    @ApiOperation("카톡 계정과 고객 연동 해제")
    @PostMapping("/disconenct")
    public CommonResponseDto<KakaoAccountDto.Res> disConnect(@RequestBody KakaoAccountDto.Req req) {
        return CommonResponseDto.ok(ktAccService.disConnect(req));
    }
}
