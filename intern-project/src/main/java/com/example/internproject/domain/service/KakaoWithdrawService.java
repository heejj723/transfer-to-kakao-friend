package com.example.internproject.domain.service;

import com.example.internproject.domain.dao.ClientRepository;
import com.example.internproject.domain.dao.KtTxRepository;
import com.example.internproject.domain.dto.KakaoWithdrawDto;
import com.example.internproject.domain.dto.dummy.CoreDummyResDto;
import com.example.internproject.domain.model.Client;
import com.example.internproject.domain.model.KtTx;
import com.example.internproject.domain.service.mock.MessageService;
import com.example.internproject.domain.service.mock.MockService;
import com.example.internproject.global.enums.BankCode;
import com.example.internproject.global.enums.BindStatus;
import com.example.internproject.global.enums.KtTxStatus;
import com.example.internproject.global.enums.ReceiverType;
import com.example.internproject.global.exception.CoreException;
import com.example.internproject.global.exception.ErrorCode;
import com.example.internproject.global.utils.EventConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class KakaoWithdrawService {

    private final KtTxRepository ktTxRepository;
    private final MessageService messageService;
    private final ClientRepository clientRepository;
    private final MockService mockService;
    private final TransferValidator transactionStatusValidator;
    private final EventConverter.TimestampConverter timestampConverter;
    private final EventConverter.TimeConverter timeConverter;
    private final Integer TX_RES_CODE_OK = 200;
    private final String OUT_MESSAGE = "간편이체";


    @Transactional(rollbackFor = Exception.class, isolation = Isolation.REPEATABLE_READ)
    public KakaoWithdrawDto.Res withdraw(KakaoWithdrawDto.Req reqDto) {
        // 1. 코어쪽 대표계좌로부터 출금 요청
        // TODO: 코어API 에러 시 Exception Handling 필요
        CoreDummyResDto coreDummyResDto =
            mockService.withdrawOk(BankCode.KAKAO.getCode(), reqDto.getOutAccount(), reqDto.getAmount());

        // 2. 이체 로그 생성
        Long txId = createKakaoTransferLog(coreDummyResDto, reqDto);

        // 3. 수취인 타입 조회
        ReceiverType receiverType = getReceiverTypeFromCore(reqDto.getInKakaoId());

        String uuid = mockService.getKakaoMsgUUID(reqDto.getInKakaoId());

        // 4. 메시지 전송 (메시지 이력은 메시징쪽에서 관리한다고 가정)
        messageService.send(
            receiverType,
            txId,
            reqDto.getInKakaoId(),
            uuid,
            reqDto.getKtInMsg(),
            reqDto.getMsgCardType(),
            reqDto.getAmount());

        return KakaoWithdrawDto.Res.builder()
            .txId(txId)
            .ktTxStatus(KtTxStatus.DEPOSIT_WAIT)
            .amount(reqDto.getAmount())
            .build();
    }

    // 이체 로그 생성
    private Long createKakaoTransferLog(CoreDummyResDto coreDummyResDto, KakaoWithdrawDto.Req kakaoWithdrawReqDto) {
        KtTx ktTx = KtTx.of(
            timestampConverter.convert(coreDummyResDto.getTxStartAt()),
            timestampConverter.convert(coreDummyResDto.getTxEndAt()),
            timeConverter.convert(coreDummyResDto.getTxTime()),
            TX_RES_CODE_OK,
            coreDummyResDto.getTxIsError(),
            coreDummyResDto.getTxErrorMsg(),
            coreDummyResDto.getTxAmount(),
            coreDummyResDto.getTxFee(),
            OUT_MESSAGE + "(" + kakaoWithdrawReqDto.getInName() + ")",
            OUT_MESSAGE + "(" + kakaoWithdrawReqDto.getInName() + ")",
            kakaoWithdrawReqDto.getInKakaoId(),
            kakaoWithdrawReqDto.getInName(),
            kakaoWithdrawReqDto.getUserId(),
            kakaoWithdrawReqDto.getOutAccount(),
            kakaoWithdrawReqDto.getKtInMsg(),
            kakaoWithdrawReqDto.getMsgCardType(),
            KtTxStatus.DEPOSIT_WAIT);
        return ktTxRepository.create(ktTx);
    }

    // 수취인이 당행 고객인지 확인
    private ReceiverType getReceiverTypeFromCore(Long inKakaoId) {
        Optional<Client> clientOptional = clientRepository.findByKtAccId(inKakaoId);

        //TODO: 옵셔널 좋게 쓰고 있는것 같지가 않은데..?

        // 카카오아이디에 매핑된 고객이 존재하지 않는다 --> NONE
        // 매핑된 고객이 존재하지만 BindStatus != Active --> Member
        // 매핑된 고객이 존재하면서 BindStatus == Active --> Client
        if (clientOptional.isPresent()) {
            return ReceiverType.NONE;
        }
        else if (!BindStatus.ACTIVE.getCode().equals(clientOptional.get().getKtBindStatus())) {
            return ReceiverType.NONE;
        } else {
            return ReceiverType.CLIENT;
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public KakaoWithdrawDto.Res cancelWithdraw(Long txId) {

        /** 트랜잭션 lock 걸기 */
        KtTx transfer = ktTxRepository.findByTxIdForOneDayForUpdateNowait(txId)
            .orElseThrow(()-> new CoreException(ErrorCode.TX_NOT_FOUND));

        transactionStatusValidator.checkDepositWait(transfer);

        mockService.depositOk(BankCode.KAKAO.getCode(),
            transfer.getOutAccount(), transfer.getTxAmount());

        // 2. 업데이트
        ktTxRepository.updateTxStatus(txId, KtTxStatus.CANCEL.getCode());

        return KakaoWithdrawDto.Res.builder()
            .txId(txId)
            .ktTxStatus(KtTxStatus.CANCEL)
            .amount(transfer.getTxAmount())
            .build();
    }
}
