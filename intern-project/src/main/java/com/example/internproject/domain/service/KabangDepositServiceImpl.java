package com.example.internproject.domain.service;

import com.example.internproject.domain.dao.ClientRepository;
import com.example.internproject.domain.dao.KtTxRepository;
import com.example.internproject.domain.dto.KakaoDepositDto;
import com.example.internproject.domain.model.Client;
import com.example.internproject.domain.model.KtTx;
import com.example.internproject.domain.service.mock.MockService;
import com.example.internproject.global.enums.KtTxStatus;
import com.example.internproject.global.exception.CoreException;
import com.example.internproject.global.exception.ErrorCode;
import com.example.internproject.global.exception.ValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class KabangDepositServiceImpl implements KakaoDepositService<KakaoDepositDto.Req, KakaoDepositDto.Res> {

    private final KtTxRepository ktTxRepository;
    private final ClientRepository clientRepository;
    private final MockService mockService;
    private final TransferValidator transferValidator;

    /** 당행 계좌로 입금*/
    @Override
    @Transactional(noRollbackFor = RuntimeException.class)
    public KakaoDepositDto.Res deposit(KakaoDepositDto.Req reqDto) throws CoreException{
        Long txId = reqDto.getTxId();

        // 1. 고객 찾기
        Client receiver = clientRepository.findByKtAccId(reqDto.getKtAccId())
            .orElseThrow(() -> new CoreException(ErrorCode.CLIENT_NOT_FOUND));

        /** 여기서부터 락 걸려있음 */
        KtTx transfer = ktTxRepository.findByTxIdForOneDayForUpdateNowait(txId)
            .orElseThrow(() -> new CoreException(ErrorCode.TX_NOT_FOUND));

        // todo: 이름 검증 부분 이렇게 처리하는거 맞나..??? try catch 없으면 어떻게 되지?
        // 2. 이름 검증
        try {
            transferValidator.validateName(transfer.getInName(), receiver.getUserName());
        } catch (ValidationException ve) {
            ktTxRepository.updateTxStatus(txId, KtTxStatus.FAILURE.getCode());
            throw ve;
        }

        // 3. CBS에 입금요청
        mockService.depositOk(transfer.getInBankCode(),receiver.getKbAccount(), transfer.getTxAmount());

        // 4. 이체 상태 변경
        ktTxRepository.updateTxStatus(txId, KtTxStatus.COMPLETE.getCode());

        return KakaoDepositDto.Res.builder()
            .txId(txId)
            .ktTxStatus(KtTxStatus.COMPLETE)
            .amount(transfer.getTxAmount())
            .build();
    }



}
