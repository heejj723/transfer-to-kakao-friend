package com.example.internproject.domain.service.mock;

import com.example.internproject.domain.dto.dummy.AccountInfoDummyDto;
import com.example.internproject.domain.dto.dummy.CoreDummyResDto;
import com.example.internproject.domain.dto.dummy.KtDummyResDto;
import com.example.internproject.global.enums.BankCode;
import com.example.internproject.global.exception.CoreException;
import com.example.internproject.global.exception.ErrorCode;
import org.springframework.stereotype.Service;

@Service
public class MockService {

    public CoreDummyResDto withdrawOk(String bankCode, String outAccount, Long money) {
        return new CoreDummyResDto("12345", "12346", "3",
            false, "", money, 0L);
    }

    public CoreDummyResDto withdrawFail(String bankCode, String inAccount, Long money) {
        return new CoreDummyResDto("12345", "12346", "3",
            true, "this is error", money, 0L);
    }

    public String getKakaoMsgUUID(Long ktAccId) {
        return "example-uuid";
    }

    // id 와 비밀번호를 입력 후 토큰을 받아오는 것까지 모두 생략. (가정)
    public KtDummyResDto getKakaoProfile() {
        return new KtDummyResDto(999L, "example@kakao.com", "rita", "/image/url");
    }

    public void depositOk(String bankCode, String inAccount, Long money) {
        ;
    }

    public AccountInfoDummyDto getAccountInfoOk(String bankCode, String account) {
        return new AccountInfoDummyDto(
            BankCode.SHINHAN, account, "정희정",9000000L, true
        );
    }

    public AccountInfoDummyDto getAccountInfoNotAvailable(String bankCode, String account) {
        return new AccountInfoDummyDto(
            BankCode.SHINHAN, account, "정희정",9000000L, false
        );
    }

    public AccountInfoDummyDto getAccountInfoFail(String bankCode, String account) {
        throw new CoreException(ErrorCode.TX_NOT_FOUND);
    }

}
