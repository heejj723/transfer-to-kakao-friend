package com.example.internproject.domain.dto;

import com.example.internproject.global.enums.KtTxStatus;
import com.example.internproject.global.enums.TxType;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
public class TransferHistoryDto {

    @Getter
    @AllArgsConstructor
    public static class Res {
        private Long txId;
        private String outMsg; // 내통장에 표시된 이체 메시지
        private KtTxStatus ktTxStatus;
        private TxType txType;
        private Long amount;
        private String txDt;
    }
}
