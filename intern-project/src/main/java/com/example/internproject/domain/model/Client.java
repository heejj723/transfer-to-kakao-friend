package com.example.internproject.domain.model;


import com.example.internproject.global.enums.BindStatus;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.sql.Timestamp;


@Getter
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class Client {

    private Long userId;
    private String userName;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    // 카톡 출금은 대표 카카오뱅크 출입금계좌가 있어야만 가능함
    private String kbAccount;
    private Long ktAccId;
    private String ktBindStatus;

    protected Client() {
    }

    public static Client of (Long userId, String userName, Timestamp createdAt,
                  Timestamp updatedAt, String kbAccount, Long ktAccId, String ktBindStatus) {
        return new Client(
            userId, userName, createdAt, updatedAt, kbAccount, ktAccId, ktBindStatus
        );
    }

    public static Client of (Long userId, Long ktAccId, String ktBindStatus) {
        return new Client(
            userId, null, null, null, null, ktAccId, ktBindStatus
        );
    }

    public static Client of (String userName, BindStatus ktBindStatus) {
        return new Client(null, userName, null, null,
            null, null, ktBindStatus.getCode());
    }
}
