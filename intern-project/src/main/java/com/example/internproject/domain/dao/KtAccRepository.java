package com.example.internproject.domain.dao;

import com.example.internproject.domain.model.KtAcc;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface KtAccRepository {


    Long create(KtAcc ktAcc);
}
