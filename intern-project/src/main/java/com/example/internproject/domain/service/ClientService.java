package com.example.internproject.domain.service;

import com.example.internproject.domain.dao.ClientRepository;
import com.example.internproject.domain.dto.ClientReqDto;
import com.example.internproject.domain.model.Client;
import com.example.internproject.global.exception.CoreException;
import com.example.internproject.global.exception.ErrorCode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.awt.print.Pageable;
import java.util.List;
import java.util.Optional;


@Service
public class ClientService{

    private final ClientRepository clientRepository;

    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Transactional
    public Long create(ClientReqDto clientReqDto) {
        return clientRepository.save(Client.of(
                clientReqDto.getUserName(),
                clientReqDto.getKtBindStatus()));
    }

    //TODO: paging 필요
    public List<Client> getAllClient() {
        return clientRepository.findAll();
    }

    //TODO: model 을 그대로 반환하는 대신 dto 형식으로 필수 정보만 주기
    public Client getClient(Long userId) {
        return clientRepository.findById(userId).orElseThrow(() -> new CoreException(ErrorCode.CLIENT_NOT_FOUND));
    }


}
