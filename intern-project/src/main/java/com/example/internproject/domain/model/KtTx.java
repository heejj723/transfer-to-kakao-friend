package com.example.internproject.domain.model;


import com.example.internproject.global.enums.KtTxStatus;
import com.example.internproject.global.enums.MsgCardType;
import lombok.*;

import java.sql.Time;
import java.sql.Timestamp;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class KtTx {

    private Long txId;
    private Timestamp createdAt;
    private Timestamp txStartAt;
    private Timestamp txEndAt;
    private Time txTime; // 소요시간
    private Integer txResCode;
    private Boolean txIsError;
    private String txErrorMsg;
    @Getter
    private Long txAmount;
    private Long txFee;
    private String txInMsg; // 받는분에게 표기
    private String txOutMsg; // 나에게 표기
    private Long inKtAccId;
    @Getter
    private String inBankCode;
    @Getter
    private String inAccount;
    @Getter
    private String inName;
    private Long outUserId;
    @Getter
    private String outAccount;
    private Timestamp ktTxRegTs;
    private Timestamp ktTxCnlTs;
    private Timestamp ktTxCplTs;
    private String ktInMsg;
    private String ktInMsgCdtp;
    @Getter
    @Setter
    private String ktTxStatus;
    private String ktTxFailReason;
    @Getter
    private Integer inAccInputTry;
    private Integer warnFl;

    public static KtTx of(
        Timestamp txStartAt,
        Timestamp txEndAt,
        Time txTime,
        Integer txResCode,
        Boolean txIsError,
        String txErrorMsg,
        Long txAmount,
        Long txFee,
        String txInMsg, // 받는분에게 표기
        String txOutMsg, // 나에게 표기
        Long inKtAccId,
        String inName,
        Long outUserId,
        String outAccount,
        String ktInMsg,
        MsgCardType ktInMsgCdtp,
        KtTxStatus ktTxStatus
    ) {
        return new KtTx(
            null, null, txStartAt, txEndAt, txTime, txResCode, txIsError,
            txErrorMsg, txAmount, txFee, txInMsg, txOutMsg, inKtAccId, "null",null, inName, outUserId,
            outAccount, null, null, null, ktInMsg, ktInMsgCdtp.getCode(),
            ktTxStatus.getCode(), null, 0, 0);
    }
}
