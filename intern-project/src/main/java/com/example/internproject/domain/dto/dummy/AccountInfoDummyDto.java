package com.example.internproject.domain.dto.dummy;

import com.example.internproject.global.enums.BankCode;
import lombok.*;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AccountInfoDummyDto {

    private BankCode bankCode;
    private String accountNumber;
    private String name;
    private Long amount;
    private Boolean available;
}
