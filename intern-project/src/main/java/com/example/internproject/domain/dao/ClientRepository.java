package com.example.internproject.domain.dao;


import com.example.internproject.domain.model.Client;
import org.apache.ibatis.annotations.Mapper;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Mapper
public interface ClientRepository {

    List<Client> findAll();

    Optional<Client> findById(Long userId);

    Optional<Client> findByKtAccId(Long ktAccId);

    Long save(Client client);

    Long findKtAccIdFromUserId(Long userId);

    void updateKtBindStatus(String bindStatus, Long userId);

    void updateNotNullParams(Client client);
}
