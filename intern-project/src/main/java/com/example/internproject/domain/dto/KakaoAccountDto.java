package com.example.internproject.domain.dto;

import com.example.internproject.global.enums.BindStatus;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class KakaoAccountDto {

    @Getter
    public static class Req {
        private Long userId;
        private String authToken;
    }

    @Getter
    @Builder
    public static class Res {
        private Long ktAccId;
        private BindStatus bindStatus;
    }
}
