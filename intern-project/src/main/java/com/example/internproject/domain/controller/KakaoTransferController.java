package com.example.internproject.domain.controller;

import com.example.internproject.domain.dto.KakaoDepositDto;
import com.example.internproject.domain.dto.KakaoWithdrawDto;
import com.example.internproject.domain.service.KakaoDepositService;
import com.example.internproject.domain.service.KakaoDepositServiceFactory;
import com.example.internproject.domain.service.KakaoWithdrawService;
import com.example.internproject.global.dto.CommonResponseDto;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/kakao-transfer")
@RequiredArgsConstructor
@ApiOperation("입금, 출금 API")
public class KakaoTransferController {

    private final KakaoWithdrawService kakaoWithdrawService;
    private final KakaoDepositServiceFactory kakaoDepositServiceFactory;

    @PostMapping("/withdraw")
    @ApiOperation("카카오톡 출금 요청 API")
    public CommonResponseDto<KakaoWithdrawDto.Res> withdraw(@RequestBody KakaoWithdrawDto.Req kakaoWithdrawReqDto) {
        return CommonResponseDto.ok(kakaoWithdrawService.withdraw(kakaoWithdrawReqDto));
    }

    @PostMapping("/withdraw/cancel")
    @ApiOperation("카카오톡 이체 취소 요청 API")
    public CommonResponseDto<KakaoWithdrawDto.Res> cancelWithdraw(@RequestParam Long txId) {
        return CommonResponseDto.ok(kakaoWithdrawService.cancelWithdraw(txId));
    }

    @PostMapping("/deposit")
    @ApiOperation("카카오톡 입금 요청 API")
    public CommonResponseDto<KakaoDepositDto.Res> deposit(
            @RequestBody KakaoDepositDto.Req kakaoDepositReqDto) {
        KakaoDepositService<KakaoDepositDto.Req, KakaoDepositDto.Res> kakaoDepositService =
            kakaoDepositServiceFactory.getService(
                    kakaoDepositReqDto.getTxId(), kakaoDepositReqDto.getReceiverType());
        return CommonResponseDto.ok(kakaoDepositService.deposit(kakaoDepositReqDto));
    }
}
