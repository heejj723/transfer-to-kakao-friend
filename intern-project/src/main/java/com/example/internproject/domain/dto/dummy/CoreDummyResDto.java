package com.example.internproject.domain.dto.dummy;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CoreDummyResDto {
    private String txStartAt;
    private String txEndAt;
    private String txTime;
    private Boolean txIsError;
    private String txErrorMsg;
    private Long txAmount;
    private Long txFee;
}
