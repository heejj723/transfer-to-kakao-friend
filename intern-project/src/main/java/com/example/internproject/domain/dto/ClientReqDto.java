package com.example.internproject.domain.dto;

import com.example.internproject.global.enums.BindStatus;
import lombok.Getter;

@Getter
public class ClientReqDto {
    public String userName;
    public BindStatus ktBindStatus = BindStatus.IN_ACTIVE;
}

