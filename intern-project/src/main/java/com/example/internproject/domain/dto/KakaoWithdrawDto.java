package com.example.internproject.domain.dto;

import com.example.internproject.global.enums.KtTxStatus;
import com.example.internproject.global.enums.MsgCardType;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class KakaoWithdrawDto {

    @Getter
    public static class Req {

        private Long userId;
        private String inName;
        private String outAccount;
        private Long amount;
        private Long inKakaoId;
        private String ktInMsg; // 카톡으로 보낼 메시지
        private MsgCardType msgCardType; // 카톡 메시지 카드 타입
    }

    @Getter
    @Builder
    public static class Res {
        private Long txId;
        private KtTxStatus ktTxStatus;
        private Long amount;
    }


}
