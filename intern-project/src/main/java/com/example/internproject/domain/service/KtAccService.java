package com.example.internproject.domain.service;

import com.example.internproject.domain.dao.ClientRepository;
import com.example.internproject.domain.dao.KtAccRepository;
import com.example.internproject.domain.dto.KakaoAccountDto;
import com.example.internproject.domain.dto.dummy.KtDummyResDto;
import com.example.internproject.domain.model.Client;
import com.example.internproject.domain.model.KtAcc;
import com.example.internproject.domain.service.mock.MockService;
import com.example.internproject.global.enums.BindStatus;
import com.example.internproject.global.exception.CoreException;
import com.example.internproject.global.exception.ErrorCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
public class KtAccService {
    private final KtAccRepository ktAccRepository;
    private final ClientRepository clientRepository;
    private final MockService mockService;

    @Transactional(rollbackFor = Exception.class)
    public KakaoAccountDto.Res connect(KakaoAccountDto.Req ktAccReqDto) {

        /** 실제로는 Access Token 등의 만료시간이 지나면 다시 로그인을 요청하겠지만,
         * 나는 우리 테이블에 kt 정보를 가지고 있으면서 (정보 삭제 없이) 연동 연결-해제만 가능하도록 하려고 함*/

        Long userId = ktAccReqDto.getUserId();
        Long ktAccId = clientRepository.findKtAccIdFromUserId(userId);

        return doConnect(userId, ktAccId);

    }

    private KakaoAccountDto.Res doConnect(Long userId, Long ktAccId) {
        //TODO: 예외처리 다양화
        // 1. 카톡ID 가 있지만 다른 계정과 연결 시도할 경우
        // 2. 카톡ID 가 있지만 현재 이용불가능한 카톡인 경우
        if (ktAccId != null) {
            clientRepository.updateKtBindStatus(BindStatus.ACTIVE.getCode(), userId);
            return KakaoAccountDto.Res.builder()
                .ktAccId(ktAccId)
                .bindStatus(BindStatus.ACTIVE)
                .build();
        }
        // 연동된 적이 한번도 없다면;
        return createKakaoAccountInfo(userId);
    }

    private KakaoAccountDto.Res createKakaoAccountInfo(Long userId) {
        KtDummyResDto ktDummyResDto = mockService.getKakaoProfile();

        Long ktAccId = ktDummyResDto.getId();
        KtAcc toCreate = KtAcc.of(
            ktAccId, ktDummyResDto.getEmail(),
            ktDummyResDto.getProfileUrl(), ktDummyResDto.getNickname(),
            userId);

        ktAccRepository.create(toCreate);

        /** client 와 연동 */
        clientRepository.updateNotNullParams(Client.of(
            userId, ktAccId, BindStatus.ACTIVE.getCode()));

        return KakaoAccountDto.Res.builder()
            .ktAccId(ktAccId)
            .bindStatus(BindStatus.ACTIVE)
            .build();
    }

    @Transactional
    public KakaoAccountDto.Res disConnect(KakaoAccountDto.Req ktAccReqDto) {
        Long userId = ktAccReqDto.getUserId();
        Client client = clientRepository.findById(userId)
            .orElseThrow(()-> new CoreException(ErrorCode.CLIENT_NOT_FOUND));
        clientRepository.updateKtBindStatus(BindStatus.IN_ACTIVE.getCode(), userId);

        return KakaoAccountDto.Res.builder()
            .ktAccId(client.getKtAccId())
            .bindStatus(BindStatus.IN_ACTIVE)
            .build();
    }
}