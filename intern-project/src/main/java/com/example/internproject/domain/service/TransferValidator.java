package com.example.internproject.domain.service;

import com.example.internproject.domain.model.KtTx;
import com.example.internproject.global.enums.KtTxStatus;
import com.example.internproject.global.exception.CoreException;
import com.example.internproject.global.exception.ErrorCode;
import com.example.internproject.global.exception.ValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TransferValidator {

    // todo: 특정 이체상태인지 확인할 수 있도록, KtTxStatus 도 같이 받는다.
    public void checkDepositWait(KtTx transfer) {
        if (!KtTxStatus.DEPOSIT_WAIT.getCode().equals(transfer.getKtTxStatus())) {
            throw new ValidationException(ErrorCode.INVALID_TX_STATUS);
        }
    }

    public void validateName(String nameBySender, String realName) {
        if (nameBySender == null || !nameBySender.equals(realName)) {
            throw new ValidationException(ErrorCode.INVALID_NAME_INPUT);
        }
    }
}
