package com.example.internproject.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class KtAcc {
    private Long ktAccId;
    private String ktEmail;
    private String profileUrl;
    private String nickname;
    private Long userId;

    public static KtAcc of(Long ktAccId, String ktEmail, String profileUrl, String nickname, Long userId) {
        return new KtAcc(
            ktAccId, ktEmail, profileUrl, nickname, userId);
    }
}