package com.example.internproject.domain.service;

public interface KakaoDepositService<Q, S> {

    S deposit(Q req);
}
