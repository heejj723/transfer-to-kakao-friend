package com.example.scheduler.dao;

import com.example.scheduler.model.KtTx;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface KtTxRepository {

    Long create24();
    Long create23();

    List<KtTx> findAllTransferAfter24();
    List<KtTx> findAllTransferAfter23();

    void updateTxStatus(Long txId, String ktTxStatus);
    KtTx findByTxIdWithLockNowait(Long txId);
    void updateWarnFl(Long txId);
}
