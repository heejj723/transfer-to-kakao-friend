package com.example.scheduler.global;

public enum KtTxStatus implements CodeEnum{
    COMPLETE("COMPLETE"),
    FAILURE("FAILURE"),
    CANCEL("CANCEL"),
    WARNED("WARNED"),
    DEPOSIT_WAIT("DEPOSIT_WAIT"),
    IN_PROGRESS("IN_PROGRESS");

    private String ktTxStatus;

    KtTxStatus(String ktTxStatus) {
        this.ktTxStatus = ktTxStatus;
    }

    @Override
    public String getCode() {
        return ktTxStatus;
    }
}
