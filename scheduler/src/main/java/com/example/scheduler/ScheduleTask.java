package com.example.scheduler;

import com.example.scheduler.dao.KtTxRepository;
import com.example.scheduler.global.KtTxStatus;
import com.example.scheduler.model.KtTx;
import com.example.scheduler.service.MessageService;
import com.example.scheduler.service.MockService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ScheduleTask {

    private final KtTxRepository repository;
    private final MessageService messageService;
    private final MockService mockService;

    @Scheduled(fixedDelay = 3000)
    @Transactional
    public void checkCancel() {

        List<KtTx> transfers = repository.findAllTransferAfter24();
        // TODO: lock 은 언제 걸어?
        transfers.forEach(t -> {
            repository.updateTxStatus(t.getTxId(), KtTxStatus.CANCEL.getCode());
        });

    }

    @Scheduled(fixedDelay = 3000)
    @Transactional
    public void checkWarning() {
        List<KtTx> transfers = repository.findAllTransferAfter23();

        transfers.forEach(t -> {
            // 수정필요: 이체건마다 외부 api 에 의존하면 매우 느려짐
            // 미리 카톡 연동할 때 uuid 를 받아두는게 낫겠음
            // uuid 를 미리 바꾸어 놓으면 휴대폰 바꿨을 경우는 어떻게해..?
            String uuid = mockService.getKakaoMsgUUID(t.getInKtAccId());
            messageService.send(t.getTxId(), t.getInKtAccId(),
                uuid, "카톡 이체를 받아주세요");
            repository.updateWarnFl(t.getTxId());
        });

    }

    @Scheduled(fixedDelay = 1000)
    public void insertTransferCancel() {
        repository.create24();
    }

    @Scheduled(fixedDelay = 1000)
    public void insertTransferWarn() {
        repository.create23();
    }

//    @Scheduled(fixedDelay = 10000)
//    public void insertBulk() {
//        for (int i = 0; i < 1000; i++) {
//            Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now().minusDays(1));
//            repository.create(KtTx.of(
//                timestamp
//            ));
//        }
//
//    }
}
