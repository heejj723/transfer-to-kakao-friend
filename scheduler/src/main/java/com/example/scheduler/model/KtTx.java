package com.example.scheduler.model;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class KtTx {

    private Long txId;
    private Timestamp createdAt;
    private Timestamp txStartAt;
    private Timestamp txEndAt;
    private Time txTime; // 소요시간
    private Integer txResCode;
    private Boolean txIsError;
    private String txErrorMsg;
    @Getter
    private Long txAmount;
    private Long txFee;
    private String txInMsg; // 받는분에게 표기
    private String txOutMsg; // 나에게 표기
    private Long inKtAccId;
    @Getter
    private String inBankCode;
    @Getter
    private String inAccount;
    @Getter
    private String inName;
    private Long outUserId;
    @Getter
    private String outAccount;
    private Timestamp ktTxRegTs;
    private Timestamp ktTxCnlTs;
    private Timestamp ktTxCplTs;
    private String ktInMsg;
    private String ktInMsgCdtp;
    @Getter
    @Setter
    private String ktTxStatus;
    private String ktTxFailReason;
    @Getter
    private Integer inAccInputTry;

    public static KtTx of() {
        return new KtTx(
            null, null, null, null, null, null, null,
            null, 10000L, 0L, null, null, 356L, "null",null, "정희정", 33L,
            "outAccount", null, null, null, null, "RANDOM",
            "DEPOSIT_WAIT", null, 0);
    }
}
