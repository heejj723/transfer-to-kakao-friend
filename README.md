# 카카오톡 친구에게 이체하기

## Flow Chart
[Flow Chart](https://wiki.kabang.io/display/~rita.jj/%5B0%5D+flow+chart)

## API 정의서 & 시퀀스 다이어그램
[API정의서 & 시퀀스 다이어그램](https://wiki.kabang.io/pages/viewpage.action?pageId=111023379)

## ERD
[ERD](https://wiki.kabang.io/display/~rita.jj/%5B3%5D+ERD)

## 데이터 추출 쿼리
[데이터추출 쿼리](https://wiki.kabang.io/pages/viewpage.action?pageId=111046369)

